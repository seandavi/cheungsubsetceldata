\documentclass{article}
\usepackage{hyperref}

\author{Sean Davis}
\title{Working with Affymetrix Expression Array Data}
\date{\today}

\begin{document}
\maketitle

\section{Introduction}

Our tasks in this tutorial are to:

\begin{itemize}
  \item{Load affymetrix data from raw .cel files}
  \item{Use exploratory data analysis to examine the raw data}
  \item{Process the array data to produce normalized data that can be used for further analysis (differential expression)}
  \item{Examine the effects of different normalization approaches}
\end{itemize}

We are going to be loading the affymetrix .CEL files from an old paper described by the abstract:

\begin{quote}
  Natural variation in gene expression is extensive in humans and other organisms, and variation in the baseline expression level of many genes has a heritable component. To localize the genetic determinants of these quantitative traits (expression phenotypes) in humans, we used microarrays to measure gene expression levels and performed genome-wide linkage analysis for expression levels of 3,554 genes in 14 large families. For approximately 1,000 expression phenotypes, there was significant evidence of linkage to specific chromosomal regions. Both cis- and trans-acting loci regulate variation in the expression levels of genes, although most act in trans. Many gene expression phenotypes are influenced by several genetic determinants. Furthermore, we found hotspots of transcriptional regulation where significant evidence of linkage for several expression phenotypes (up to 31) coincides, and expression levels of many genes that share the same regulatory region are significantly correlated. The combination of microarray techniques for phenotyping and linkage analysis for quantitative traits allows the genetic mapping of determinants that contribute to variation in human gene expression. \cite{morley}
\end{quote}


\subsection{Installation of the CheungSubsetCelData Package}

This tutorial package can be installed into R using the following command (paste into R)

<<eval=FALSE>>=
install.packages('CheungSubsetCelData',
  contriburl=contrib.url('http://watson.nci.nih.gov/~sdavis/software/R',
  type='source'),type='source')
@ 


\section{Getting Started}

We are going to be using one of several packages for dealing with affymetrix data, the \texttt{affy} package.  This package is applicable to 3'-biased arrays we will be using here.  Other packages dealing with affymetrix arrays can be found at this url: \url{http://www.bioconductor.org/help/workflows/arrays/}.

\subsection{Install the affy package}

Affymetrix data are stored on the disk in a single file per sample in a format called .CEL.  This format can be either binary or text.  Thankfully, there is a Bioconductor package, the \texttt{affy} package \cite{gautier}, that knows all about .CEL files and how to load them.  We will be installing the \texttt{affy} package as a first step.

<<installaffy,eval=false>>=
library(BiocInstaller)
biocLite('affy')
@ 

Remember that if you have not installed Bioconductor base packages first, the above command may fail.  If that happens, head back to the Bioconductor website and start there.

<<loadaffy>>=
library(affy)
library(CheungSubsetCelData)
@ 

To get an overview of the affy package, use:

<<helpaffy,eval=false>>=
help(package='affy')
@ 

\subsection{Finding the .CEL files}

The .CEL files for this tutorial are stored in the ``CheungSubsetCelData'' package; packages like this one that contain mainly data and not functionality are called ``data packages'' and are a nice way to simplify management for R.

Once an R package is installed, R can find files in the package using the \texttt{system.file()} function.  Use the R help system to read a bit about \texttt{system.file}, but the directory that stores the .CEL files is here:

<<getdir>>=
celfilepath = system.file('extdata',package='CheungSubsetCelData')
celfilepath
@ 

The \texttt{celfilepath} is the directory on the disk where the .CEL files are located.  Use the \texttt{list.celfiles} function to list the .CEL files in that directory.

<<listcelfiles>>=
list.celfiles(celfilepath)
@ 

Read the help page for the \texttt{ReadAffy} function and apply it to the .CEL files in the \texttt{celfilepath} location.

<<readcelfiles>>=
abatch = ReadAffy(filenames=list.celfiles(celfilepath),
    celfile.path=celfilepath,compress=TRUE)
@ 

In order to interpret the array features, the affy package needs to gain access to the ``content design file'' or CDF.  Bioconductor has data packages available for many array types that encapsulate the design information.  As soon as the affy package needs some information from the data package, it will automatically download it.  

<<lookataffybatch>>=
abatch
@ 

The \texttt{annotation} accessor function is used to determine the array type after loading affy data into R.  In this case, the array type is the ``hgfocus'' array.  \textit{Note that the information stored in the AffyBatch object is ``raw data''.  It has not been summarized to probeset, yet, for example}.  

\section{Exploratory Data Analysis}

The first step after loading microarray data is to do some data exploration.  

\textbf{Exercise 1}:  Read the help for ``AffyBatch'' to find an accessor to give you the ``pm'' (perfect match) intensities.  Make a text summary of the resulting matrix.

<<exercise1,echo=false,results=hide>>=
summary(pm(abatch))
@ 

\subsection{histograms}

\textbf{Exercise 2}:  Create a matrix of the \texttt{pm} intensities.  Then, make a histogram of the intensities for the first array.  Is this plot useful to you?  How could you improve it?  (Hint: you might want to transform the data before plotting).

<<exercise2,results=hide,echo=false>>=
pmmat = pm(abatch)
hist(pmmat[,1])
hist(log2(pmmat[,1]))
@ 

\textbf{Exercise 3}:  Instead of a histogram, make a density plot.  You may want to apply the same trick you applied above.

<<exercise3,results=hide,echo=false>>=
pmmat = pm(abatch)
plot(density(pmmat[,1]))
@ 

\textbf{Exercise 4}:  Look at the help for the \texttt{plotDensity.AffyBatch} function and use the function to compare the densities for all the arrays.  Do the arrays have similar intensity distributions?  

<<exercise4,echo=false,results=hide>>=
plotDensity.AffyBatch(abatch)
@ 

\subsection{boxplots}

In addition to density plots and histograms, a boxplot can be quite useful for examine at a high level the relative distributions between samples.  

\textbf{Exercise 5}:  Create a boxplot using the ``pm'' intensity matrix.  Again, is this helpful, or do you need to transform the data?  Do these intensity distributions appear to be similar to each other?  As an aside, what happens if you call ``boxplot'' on the AffyBatch object directly?  

<<exercise5,echo=false,results=hide>>=
boxplot(pmmat)
boxplot(log2(pmmat))
boxplot(abatch)
@ 

\subsection{Scatterplots}

We can compare arrays in a pairwise fashion to look for similarity and for nonlinear effects.  

\textbf{Exercise 6}: Make a \texttt{pairs} plot of the first four samples in the AffyBatch object.  Hint: the AffyBatch class has a method for the \texttt{pairs} method.  Do any samples in the pairs plot look unusual or problematic?

<<exercise6,results=hide,echo=false>>=
pairs(abatch[,1:4])
@ 

\subsection{MA plots}

Remember that MA plots are useful to:

\begin{itemize}
  \item{Compare arrays to each other}
  \item{Look for linear and non-linear trends that need to be corrected}
\end{itemize}

\textbf{Exercise 7}:  Look at the help for the affy package to find a method that creates an MA plot (or plots) and use it to look at the MA plots for the first four arrays.  Do you see any effects that appear to need some correction?

<<exercise7,results=hide,echo=false>>=
MAplot(abatch[,1:4])
@ 

\textbf{Exercise 8}:  Try using the \texttt{Mbox} method on your AffyBatch object.  What is it showing you?  

<<exercise8,results=hide,echo=false>>=
Mbox(abatch)
@ 

\subsection{Image Plots}

Microarrays are physical media that are subject to scratches, defects, and other systematic problems that may show up as a recognizable problem on the array.  Since modern microarrays store the position location of the intensities on the physical array, we can plot the data as it was arranged on the array.  This is not a routine plot, but it can be useful to check for arrays that appear to be significant outliers or problems.

<<fig=true>>=
image(abatch[,1],main='First array')
@ 


\section{Normalization}

We are going to ignore the background subtraction step for the purposes of this vignette and move along to normalization.  The goal of normalization is to remove as much technical bias as possible while conserving biological variability.  There are a number of methods for normalization in the affy package.  The \texttt{normalize.methods} function returns the possible normalization methods that we can apply to our raw AffyBatch dataset.

<<>>=
# ask about the methods for normalization for our AffyBatch object
normalize.methods(abatch)
@ 

Our goal in this section is to try at least one normalization method and then to evaluate the changes with regard to reducing some of the problems we saw in the raw data.  

\subsection{Apply a normalization method}

Here, I am going to apply ``quantiles'' normalization to our raw AffyBatch object; ``quantiles'' normalization is the default.  

<<normbatch>>=
normbatch = normalize(abatch,method='quantiles.robust')
class(normbatch)
normbatch
@ 

The object returned by our normalization is itself an AffyBatch object, so all the work done in the section above working with the raw AffyBatch object will pay off to examine the new normalized AffyBatch object.

\textbf{Exercise 8}:  Make a density plot, Mbox plot, and some MA plots of the normalized data.  Are the problems that you noted with the raw AffyBatch object improved or removed?

<<exercise8,results=hide,echo=false>>=
plotDensity.AffyBatch(normbatch)
Mbox(normbatch)
par(mfrow=c(2,2))
MAplot(normbatch[,1:4])
@ 


\section{Getting an ExpressionSet}

Remember that preprocessing involves the following four steps:

\begin{enumerate}
  \item{Background correction}
  \item{Normalization}
  \item{Perfect-match/mismatch correction}
  \item{Summarization}
\end{enumerate}

The affy package offers a function called \texttt{expresso} that allows custom combinations of choices for each step.  The available options for each step are given by the respective lookup functions.

<<>>=
bgcorrect.methods()
normalize.methods(abatch) # this one takes an object as an argument--???
pmcorrect.methods()
express.summary.stat.methods()
@ 

The most popular approach for working with affymetrix data is to use rma.  The choices for each step of the RMA approach are given in Table \ref{tab:rma}.

\begin{table}
\begin{centering}
  \begin{tabular}{c | c}
  Step & Method \\
  \hline
  Background Correction & RMA \\
  Normalization & quantiles \\
  PM/MM correction & pmonly \\
  Summarization & median polish \\
  \hline
  \end{tabular}
  \label{tab:rma}
  \caption{RMA settings for the steps of preprocessing of affy arrays}
\end{centering}
\end{table}

Applying rma to the abatch object is very straightforward, though, as the affy package has a wrapper for rma.  

<<rma>>=
eset = rma(abatch)
@ 

I have also created some sample information stored in the data object ``samples''.  Here, I am going to assign it to our eset object pData slot.

<<eset>>=
data(samples)
head(samples)
pData(eset)=samples
sampleNames(eset)=paste0(samples$Patient,samples$rep)
@ 

These samples are actually pairs of replicates.  If we want to look at the high-level structure of the data, we can use a simple hierarchical clustering.  Here is one example.  Feel free to modify it to try other clustering distance metrics or linkage methods.

<<fig=true>>=
plot(hclust(dist(t(exprs(eset)))))
@ 

One of the pairs does not cluster together, so we may need to spend some time sorting that detail out; it may be a technical issue, but it may also be a sample naming mixup.  Since these samples came from a larger cohort of patients, such sample labeling errors are not uncommon.

Feel free to experiment further with clustering and data exploration.  Once we are satisfied that we have sorted out data quality issues,  with an ExpressionSet in hand, we can move forward to other tasks such as hypothesis testing.


\begin{thebibliography}{1}

\bibitem{morley} Morley M, Molony CM, Weber TM, Devlin JL, Ewens KG, Spielman RS, Cheung VG.
Genetic analysis of genome-wide variation in human gene expression. Nature. 2004 
Aug 12;430(7001):743-7. PubMed PMID: 15269782.

\bibitem{gautier}   Gautier, L., Cope, L., Bolstad, B. M., and Irizarry, R. A. 2004.
  affy---analysis of Affymetrix GeneChip data at the probe level.
  Bioinformatics 20, 3 (Feb. 2004), 307-315.


\end{thebibliography}

\end{document}
